
import java.util.StringTokenizer;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   public TreeNode(String n, TreeNode c, TreeNode s) {
      setName(n);
      setFirstChild(c);
      setNextSibling(s);
   }

   public void setName(String n) {
      this.name = n;
   }

   public String getName() {
      return this.name;
   }

   public void setFirstChild(TreeNode d) {
      this.firstChild = d;
   }

   public TreeNode getFirstChild() {
      return this.firstChild;
   }

   public void setNextSibling(TreeNode r) {
      this.nextSibling = r;
   }

   public TreeNode getNextSibling() {
      return this.nextSibling;
   }

   public boolean hasSibling() {
      return (getNextSibling() != null);
   }

   public boolean hasChildren() {
      return (getFirstChild() != null);
   }

   public static void testInput(String s) { // Method to test user input string

      String[] testArray = s.split("");

      if (testArray[1].matches("[\\s\\(]")) throw new RuntimeException("Main root in equation " + s + " must not be empty");

      int countLeftPar = 0;
      int countRightPar = 0;
      int countComma = 0;

      for (int i = 1; i < testArray.length; i++) {

         if (testArray[i].matches("\\s")) throw new RuntimeException("Equation " + s + " must not contain space or tab characters");

         if (testArray[i].matches("\\(")) countLeftPar++;
         if (testArray[i].matches("\\)")) countRightPar++;
         if (testArray[i].matches(",")) countComma++;
         if (countLeftPar < countRightPar) throw new RuntimeException("Equation " + s + " unbalanced. Parenthesis must be opened with \"(\" before closing with \")\"");

         if (testArray[i].matches("\\(") && testArray[i+1].matches("\\)")) throw new RuntimeException("Equation " + s + " must not have empty parenthesis");
         if (testArray[i].matches("\\(") && testArray[i+1].matches("\\(")) throw new RuntimeException("Root in equation " + s + " must not be empty (two consecutive left parenthesis not allowed)");
         if (testArray[i].matches(",") && testArray[i+1].matches(",")) throw new RuntimeException("Two consecutive commas in equation " + s + " not allowed");
         if (testArray[i].matches("\\(") && testArray[i+1].matches("[,;:]")) throw new RuntimeException("Illegal root name \"" + testArray[i+1] + "\" in equation " + s + ".");
         if ((countLeftPar - countRightPar) == 0 && testArray[i].matches(",")) throw new RuntimeException("Illegal equation: " + s + ". Main root must not have siblings i.e. a tree must not have two main roots");
      }

      if (countLeftPar != countRightPar) throw new RuntimeException("Opened parenthesis must be closed in equation: " + s);
      if (countLeftPar == 0 && countComma != 0) throw new RuntimeException("Illegal input string " + s + ". Children of roots must be enclosed in parenthesis");

   }

   public static TreeNode parsePrefix(String s) {

      testInput(s); // Test input for correct equation form before parsing string

      StringTokenizer str = new StringTokenizer(s, "(),", true);

      TreeNode rootNode = new TreeNode("", null, null);

      TreeNode mainRoot = rootNode;

      int level = 0;

      while (str.hasMoreTokens()) {

         String temp = str.nextToken().trim();

         if (temp.equals("(")) {
            temp = str.nextToken().trim();
            rootNode.setFirstChild(new TreeNode(temp, null, null));
            rootNode = rootNode.getFirstChild();
            level++;

         } else if( temp.equals(")")) {
            rootNode = mainRoot;
            for (int i = 1; i < level; i++) {
               rootNode = rootNode.getFirstChild();
               while (rootNode.hasSibling()) {
                  rootNode = rootNode.getNextSibling();
               }
            }
            level--;

         } else if (temp.equals(",")) {
            temp = str.nextToken().trim();
            rootNode.setNextSibling(new TreeNode(temp, null, null));
            rootNode = rootNode.getNextSibling();

         } else {
            rootNode.setName(temp);
         }
      }
      return mainRoot;
   }

   public String rightParentheticRepresentation() {

      StringBuilder b = new StringBuilder();

      if (hasChildren()) {
         b.append("(");
         b.append(getFirstChild().rightParentheticRepresentation());
         b.append(")");
      }

      b.append(getName());

      if (hasSibling()) {
         b.append(",");
         b.append(getNextSibling().rightParentheticRepresentation());
      }

      return b.toString();
   }

   public static void main(String[] param) {
      //String s = "A(B1,C,D)";
      String s = "+(*(-(2,1),4),/(6,3))";
      //String s = "A(B),C(D)";
      TreeNode t = TreeNode.parsePrefix(s);
      String v = t.rightParentheticRepresentation();
      System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A

      /*
      Infikskuju prioriteedisulgudega:  (5 - 1) * 7 + 6 / 3
      Prefikskuju sulgudega:  + (* (- (5, 1), 7), / (6, 3))
      Postfikskuju sulgudega:  (((5, 1)-, 7)*, (6, 3)/ )+
      Pööratud poola kuju:  5 1 - 7 * 6 3 / +

      Vasakpoolne suluesitus
      A(B(D(G,H),E,F(I)),C(J))

      Parempoolne suluesitus
      (((G,H)D,E,(I)F)B,(J)C)A

      Kahendpuu läbimine:

      Eesjärjestus: ABDECF
      Lõppjärjestus: DEBFCA
      Keskjärjestus: DBEAFC

      */

   }
}

